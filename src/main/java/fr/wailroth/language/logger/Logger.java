package fr.wailroth.language.logger;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 26/07/2021.
Ask me to use this code, at wailroth#4736.
**/


import fr.wailroth.language.utils.Color;
import java.util.Calendar;
import java.util.Date;

/**
 * The type Logger.
 */
public class Logger {


    /**
     * The Name.
     */
    private static String name;

    /**
     * Instantiates a new Logger.
     *
     * @param name the name
     */
    public Logger(String name) {
        this.name = name;
    }

    /**
     * Log.
     *
     * @param level   the level
     * @param message the message
     */
    public static void log(Level level, Object message) {

        if(name.isEmpty()) {
            System.out.println(Color.RED_BACKGROUND +
                    "[CRITICAL] Logger : " + "Name is indefined. Please define it." +
                    Color.RESET);
            return;
        }

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        //"["+cal.get(Calendar.HOUR_OF_DAY) + ":"+ cal.get(Calendar.MINUTE) +  ":"+
        //                        cal.get(Calendar.SECOND) + "] " +

        switch (level) {
            case FINE:
                System.out.println(Color.BLUE +
                        "[FINE] "+ name + " : " + message +
                        Color.RESET);
                break;
            case INFO:
                System.out.println(Color.YELLOW +
                        "[INFO] "+ name + " : " + message +
                        Color.RESET);
                break;
            case CRITICAL:
                System.out.println(Color.RED_BACKGROUND +
                        "[CRITICAL] "+ name + " : " + message +
                        Color.RESET);
                break;

            case DEBUG:
                System.out.println(Color.CYAN_BACKGROUND +
                        "[DEBUG] "+ name + " : " + message +
                        Color.RESET);
                break;

            case ERROR:
                System.out.println(Color.RED +
                        "[ERROR] "+ name + " : " + message +
                        Color.RESET);
                break;
        }
    }

    /**
     * Debug.
     *
     * @param message the message
     */
    public static void debug(Object message) {
        log(Level.DEBUG, message);
    }

    /**
     * Error.
     *
     * @param message the message
     */
    public static void error(Object message) {
        log(Level.ERROR, message);
    }

    /**
     * Info.
     *
     * @param message the message
     */
    public static void info(Object message) {
        log(Level.INFO, message);
    }

    /**
     * Critical.
     *
     * @param message the message
     */
    public static void critical(Object message) {
        log(Level.CRITICAL, message);
    }

    /**
     * Fine.
     *
     * @param message the message
     */
    public static void fine(Object message) {
        log(Level.FINE, message);
    }


    /**
     * Gets name.
     *
     * @return the name
     */
    public static String getName() {
        return name;
    }
}


