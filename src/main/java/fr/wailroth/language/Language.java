package fr.wailroth.language;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 12/07/2021.
Ask me to use this code, at wailroth#4736.
**/



import fr.wailroth.language.logger.Level;
import fr.wailroth.language.logger.Logger;
import fr.wailroth.language.utils.FileUtil;
import fr.wailroth.language.utils.JsonManager;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * The type Language.
 */
public class Language {

    /**
     * The Lang list.
     */
    private final List<Lang> langList = new ArrayList<>();


    /**
     * The Instance.
     */
    static Language instance;


    /**
     * Get language instance
     *
     * @return language instance
     */
    public static Language getInstance() {
        if(instance == null) {
            instance = new Language();
        }

        return instance;
    }


    /**
     * Get translation
     * Default fallback in us_US
     *
     * @param key    translation key
     * @param locale locale
     * @param value  default translation
     * @return String translation
     */
    public String getTranslation(String key, Locale locale, String value) {
        String trans = null;


        for(Lang lang : langList) {

            if(!lang.getLocale().equals(locale)) {
                continue;
            }

            Logger.debug("Locale: " + (lang.getLocale() == locale));


                if(lang.getKeyTranslation().get(key) != null) {
                    trans = lang.getKeyTranslation().get(key);
                    break;
                } else {
                    trans = lang.getFallback(key);
                }

            if(trans == null) {
                lang.getKeyTranslation().put(key, value);
                trans = value;
                break;
            }

        }


        if(trans == null) {
            Logger.error("Translation error: " + key);
            return "§cTranslation error";
        }

        return trans;

    }


    /**
     * Load translations files
     *
     * @throws IOException file exception
     */
    public void loadTranslation(String path) throws IOException {

        File dir = new File(path);


        if(!dir.exists())  {
            dir.mkdirs();
        }


        File[] languageFiles = dir.listFiles();

        if (!dir.isDirectory()) {
            Logger.log(Level.CRITICAL, "Unable to find language folder.");
            return;
        }


        if(languageFiles.length == 0) {
            Logger.log(Level.ERROR, "No languages found in the language folder. Creating one...");

            Map<String, String> translation = new HashMap<>();
            translation.put("Default", "Default");

            Lang lang = new Lang("English", Locale.ENGLISH, translation);


            FileUtil.save(new File("WLib/languages/English.json"),
                    JsonManager.getInstance().serialize(lang));

            return;
        }



        for (int i = 0; i < languageFiles.length; i++) {

            if (!languageFiles[i].isFile()) {
                continue;
            }

            Lang lang = JsonManager.getInstance().deserialize(FileUtil.loadContent(languageFiles[i]), Lang.class);
                        Locale locale = lang.getLocale();

            if (locale == null) {
                Logger.log(Level.ERROR, "Language file error " +
                        ": Unable to retrieve locale" + languageFiles[0].getName() + " please, check json.");
                continue;
            }

            langList.add(lang);
            Logger.log(Level.FINE, lang.getName() + " was loaded.");

        }

        Logger.debug(langList.size());
    }

    /**
     * Save languages
     *
     * @throws IOException the io exception
     */
    public void saveLanguages(String path) throws IOException {
        File dir = new File( path);

        if (!dir.isDirectory()) {
            Logger.log(Level.CRITICAL, "Unable to find language folder.");
            return;
        }

        for(Lang lang : langList) {
            File langFile = new File( "WLib/languages/" + lang.getName()
                    + ".json");

            if(!langFile.exists()) {
                Logger.critical("Language "  + lang.getName() + " wasn't found. The file name must be the same that " +
                        "'name' field in JSON file.");
                continue;
            }

            FileUtil.save(langFile, JsonManager.getInstance().serialize(lang));

            Logger.fine(lang.getName() + " lang was successfully saved.");

        }

    }

    /**
     * Get lang list
     *
     * @return lang list
     */
    public List<Lang> getLangList() {
        return langList;
    }
}


