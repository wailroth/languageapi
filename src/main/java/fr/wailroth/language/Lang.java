package fr.wailroth.language;

/*
This code is owned by Alexis Dumain, aliase wailroth.
This file was created the 25/07/2021.
Ask me to use this code, at wailroth#4736.
**/


import java.util.Locale;
import java.util.Map;

/**
 * The type Lang.
 */
public class Lang {

    /**
     * The Name.
     */
    private final String name;
    /**
     * The Locale.
     */
    private final Locale locale;
    /**
     * The Key translation.
     */
    private final Map<String, String> keyTranslation;


    /**
     * Instantiates a new Lang.
     *
     * @param name           the name
     * @param locale         the locale
     * @param keyTranslation the key translation
     */
    public Lang(String name, Locale locale, Map<String, String> keyTranslation) {
        this.name = name;
        this.locale = locale;
        this.keyTranslation = keyTranslation;
    }

    /**
     * Gets locale.
     *
     * @return the locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets key translation.
     *
     * @return the key translation
     */
    public Map<String, String> getKeyTranslation() {
        return keyTranslation;
    }


    /**
     * Gets fallback.
     *
     * @param key the key
     * @return the fallback
     */
    public String getFallback(String key) {

        String fallback = null;

        for(Lang lang : Language.getInstance().getLangList()) {
            if(lang.getLocale() != Locale.ENGLISH) {
                continue;
            }

            fallback = lang.getKeyTranslation().get(key);


        }

        return fallback;

    }
}


